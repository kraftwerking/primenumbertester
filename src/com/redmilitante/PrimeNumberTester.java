package com.redmilitante;

/**
 * @author red
 * @version 1.0
 */
public class PrimeNumberTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// A prime number is a number that can only be divisible by 1 and
		// itself. Create a method that tests a number to see if it is prime.
		// Make it as high performance as you can. Below is a list of the first
		// prime numbers you can use for testing: 2. 3. 5. 7. 11. 13. 17. 19.
		// 23. 29. 31. 37. 41. 43. 47. 53. 59. 61. 67. 71. 73. 79. 83. 89. 97.
		// 101.

		boolean prime = false;

		for (int number = 1; number <= 101; number++) {
			prime = isPrime(new Long(number));
			if (prime) {
				System.out.println(number + ". ");
			}
		}
	}

	/**
	 * Determine if the passed in Long is a prime number
	 * 
	 * @param number
	 *            to check
	 * @return true if it is prime else false
	 */
	public static synchronized boolean isPrime(Long number) {

		boolean prime = true;

		// A more advanced version would be to use the Sieve of Eratosthenes,
		// and eliminate every not-prime.
		// But since you're checking a single number and don't want to generate
		// a new data structure to hold not-primes to check against, all sieve
		// approaches will generate too much data.

		// aks primality is the most efficient algorithm for a single number
		// http://www.math.dartmouth.edu/~carlp/PDF/complexity12.pdf
		// There's a java implementation of aks primality, it is complex and
		// involves multiple methods and dependencies, and I don't think I could
		// improve upon it
		// http://www.angelusworld.com/software.php?lang=en&num=12

		// 1 is not a prime by definition
		// skip 2, modulo 2 == 0
		if (number == 2) {
			return prime;
		}

		if ((number == 1) || (number % 2 == 0)) {
			prime = false;
			return prime;
		}

		// don't have to check if > sqrt of number + 1
		int top = (int) Math.sqrt(number) + 1;

		for (int i = 3; i < top; i += 2) {
			if (number % i == 0) {
				prime = false;
				break;
			}
		}

		return prime;
	}
}
